import java.util.ArrayList;
import java.util.Random;

public class BattleField {

   public static final int FIELD_SIZE = 10;
   public static final int MAX_SHIP_SIZE = 4;
   public ArrayList<ArrayList<Integer>> battleField;
   public ArrayList<ArrayList<Integer>> ships;
   public int shipNum = 0;

   public BattleField() {
       battleField = new ArrayList<ArrayList<Integer>>();
       for (int i = 0; i < FIELD_SIZE; i++) {
           battleField.add(new ArrayList<Integer>());
           for (int j = 0; j < FIELD_SIZE; j++) {
               battleField.get(i).add(j, 0);
           }
       }
   }

    public void buildAShip(int length){
        ArrayList<Integer> insertList = new ArrayList<Integer>();
        for (int i = 0; i < length; i++) {
            insertList.add(i, 1);
        }
        ships.add(insertList);

    }

    public void initShips(){
        ships = new ArrayList<ArrayList<Integer>>();
        for (int i= MAX_SHIP_SIZE; i>0; i--){
            for (int j = 1; j < i+1; j++) {
                buildAShip(j);
            }


        }
    }



    public boolean isAbleToPlace (int x, int y){
        boolean  able = true;
            try {
                if ((x > battleField.size()) | (y > battleField.size()) | (x < 0) | (y < 0) ) {
                    able = false;
                }
                else {
                    for (int i = x - 1; i <= x + 1; i++) {
                        for (int j = y - 1; j <= y + 1; j++){
                            if (battleField.get(i).get(j) == 1)
                                able = false; }
                    }

                }
            }
            catch (IndexOutOfBoundsException ignored){
            }

        return able;
    }

    public int randomVal(int arr[]){
        Random r = new Random();
        return  arr[r.nextInt(arr.length)];
    }
    public void randPos(){}

    public void   setShips() {
        while (shipNum < ships.size()) {

//            boolean able = true;
            int x = randomInt(0, FIELD_SIZE);
            int y = randomInt(0, FIELD_SIZE);
            int vertical = 0;
            int horizontal = 1;
            int direction = randomInt(vertical, horizontal);

            if (isAbleToPlace(x, y)) {
                for (int i = shipNum; i < ships.size(); i++) {
                    boolean able = false;

                    if (direction == vertical) {
                        for (int j = 0; j < ships.get(i).size(); j++) {
                            if (x>battleField.size()) break;
                            if (battleField.get(x).get(y) == 1) {
                                able = false;
                                x = x + 1;
                            }
                            else able = true;
                        }
                        if (able) {
                            for (int j = 0; j < ships.get(i).size(); j++) {
                                battleField.get(x).set(y, ships.get(i).get(j));
                                x = x + 1;
                            }
                            shipNum = shipNum + 1;
                        }

                    }
                    else {
                        for (int j = 0; j < ships.get(i).size(); j++)
                            if (y>battleField.size()) break;
                        if (battleField.get(x).get(y) == 1) {
                            able = false;
                            y = y + 1;
                                } else { able = true;
                        }
                        if (able) {
                            for (int j = 0; j < ships.get(i).size(); j++) {
                                battleField.get(x).set(y, ships.get(i).get(j));
                                y =y +1;
                            }
                            shipNum = shipNum + 1;
                        }
                    }
                }
            }
        }
    }


    public int randomInt(int min, int max){
        return min + (int)(Math.random() * ((max - min) + 1));
    }


    public void printShips () {
        initShips();
        for (ArrayList<Integer> item : ships){
            System.out.println();
            for (Integer i : item )
                System.out.print(i + " ");
        }
//        System.out.print(battleField.get(3).get());
    }

    public void printField () {
//        battleField.get(3).set(3, 1);
//        battleField.get(2).set(2, 1);
        for (ArrayList<Integer> item : battleField){
            System.out.println();
                for (Integer i : item )
                    System.out.print(i + " ");
            }
//        System.out.println(isAbleToPlace(0, 0));
    }
}




